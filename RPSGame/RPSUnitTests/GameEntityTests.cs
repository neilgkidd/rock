﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPSGame;
using static RPSGame.GameHelper;

namespace RPSUnitTests
{
    [TestClass]
    public class GameEntityTests
    {        
        [TestMethod]
        public void GetRandomGameEntityTest()
        {
            var gameEntry = GameHelper.GetRandomGameEntry();

            Assert.IsNotNull(gameEntry);
        }
        
        [TestMethod]
        public void GeneratedGameEntityTest()
        {
            GameEntity gameEntity = new GameEntity();
            gameEntity.Player1GameEntry = GameHelper.GetRandomGameEntry();
            gameEntity.Player2GameEntry = GameHelper.GetRandomGameEntry();

            Assert.IsNotNull(gameEntity);
        }
       
        [TestMethod]
        public void UserDefinedGameEntityTest()
        {
            GameEntity gameEntity = new GameEntity();
            gameEntity.Player1GameEntry = GameEntry.Rock;
            gameEntity.Player2GameEntry = GameHelper.GetRandomGameEntry();

            Assert.IsNotNull(gameEntity);
        }
        
        [TestMethod]
        public void RockVsPaperTest()
        {
            GameEntity gameEntity = new GameEntity();
            gameEntity.Player1GameEntry = GameEntry.Rock;
            gameEntity.Player2GameEntry = GameEntry.Paper;
            ProcessGame processGame = new ProcessGame(gameEntity);
            var winner = processGame.FindWinner();

            // paper should win
            Assert.IsTrue(winner == Result.Player2);
        }

        [TestMethod]
        public void RockVsScissorsTest()
        {
            GameEntity gameEntity = new GameEntity();
            gameEntity.Player1GameEntry = GameEntry.Rock;
            gameEntity.Player2GameEntry = GameEntry.Scissors;
            ProcessGame processGame = new ProcessGame(gameEntity);
            var winner = processGame.FindWinner();

            // rock should win
            Assert.IsTrue(winner == Result.Player1);
        }

        [TestMethod]
        public void PaperVsScissorsTest()
        {
            GameEntity gameEntity = new GameEntity();
            gameEntity.Player1GameEntry = GameEntry.Paper;
            gameEntity.Player2GameEntry = GameEntry.Scissors;
            ProcessGame processGame = new ProcessGame(gameEntity);
            var winner = processGame.FindWinner();

            // scissors should win
            Assert.IsTrue(winner == Result.Player2);
        }

        [TestMethod]
        public void PaperVsRockTest()
        {
            GameEntity gameEntity = new GameEntity();
            gameEntity.Player1GameEntry = GameEntry.Paper;
            gameEntity.Player2GameEntry = GameEntry.Rock;
            ProcessGame processGame = new ProcessGame(gameEntity);
            var winner = processGame.FindWinner();

            // paper should win
            Assert.IsTrue(winner == Result.Player1);
        }

        [TestMethod]
        public void ScissorsVsRockTest()
        {
            GameEntity gameEntity = new GameEntity();
            gameEntity.Player1GameEntry = GameEntry.Scissors;
            gameEntity.Player2GameEntry = GameEntry.Rock;
            ProcessGame processGame = new ProcessGame(gameEntity);
            var winner = processGame.FindWinner();

            // rock should win
            Assert.IsTrue(winner == Result.Player2);
        }

        [TestMethod]
        public void ScissorsVsPaperTest()
        {
            GameEntity gameEntity = new GameEntity();
            gameEntity.Player1GameEntry = GameEntry.Scissors;
            gameEntity.Player2GameEntry = GameEntry.Paper;
            ProcessGame processGame = new ProcessGame(gameEntity);
            var winner = processGame.FindWinner();

            // scissors should win
            Assert.IsTrue(winner == Result.Player1);
        }

        [TestMethod]
        public void RockVsRockTest()
        {
            GameEntity gameEntity = new GameEntity();
            gameEntity.Player1GameEntry = GameEntry.Rock;
            gameEntity.Player2GameEntry = GameEntry.Rock;
            ProcessGame processGame = new ProcessGame(gameEntity);
            var winner = processGame.FindWinner();

            // should be a draw
            Assert.IsTrue(winner == Result.Draw);
        }

        [TestMethod]
        public void PaperVsPaperTest()
        {
            GameEntity gameEntity = new GameEntity();
            gameEntity.Player1GameEntry = GameEntry.Paper;
            gameEntity.Player2GameEntry = GameEntry.Paper;
            ProcessGame processGame = new ProcessGame(gameEntity);
            var winner = processGame.FindWinner();

            // should be a draw
            Assert.IsTrue(winner == Result.Draw);
        }

        [TestMethod]
        public void ScissorsVsScissorsTest()
        {
            GameEntity gameEntity = new GameEntity();
            gameEntity.Player1GameEntry = GameEntry.Scissors;
            gameEntity.Player2GameEntry = GameEntry.Scissors;
            ProcessGame processGame = new ProcessGame(gameEntity);
            var winner = processGame.FindWinner();

            // should be a draw
            Assert.IsTrue(winner == Result.Draw);
        }

        [TestMethod]
        public void DrawLabelTest()
        {
            Assert.IsTrue(Result.Draw.GetFriendlyString() == "Draw");
        }

        [TestMethod]
        public void Player1WinLabelTest()
        {
            Assert.IsTrue(Result.Player1.GetFriendlyString() == "Player 1 Wins!");
        }

        [TestMethod]
        public void Player2WinLabelTest()
        {
            Assert.IsTrue(Result.Player2.GetFriendlyString() == "Player 2 Wins!");
        }
    }
}
