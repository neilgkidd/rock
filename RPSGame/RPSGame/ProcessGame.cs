﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RPSGame.GameHelper;

namespace RPSGame
{
    public class ProcessGame
    {
        private IGameEntity gameEntity;
        public ProcessGame(IGameEntity ge)
        {
            gameEntity = ge;
        }

        public Result FindWinner()
        {
            if (gameEntity.Player1GameEntry != gameEntity.Player2GameEntry)
            {
                switch (gameEntity.Player1GameEntry)
                {
                    case GameEntry.Rock:
                        if (gameEntity.Player2GameEntry == GameEntry.Paper)
                            return Result.Player2;
                        else if (gameEntity.Player2GameEntry == GameEntry.Scissors)
                            return Result.Player1;
                        break;
                    case GameEntry.Paper:
                        if (gameEntity.Player2GameEntry == GameEntry.Rock)
                            return Result.Player1;
                        else if (gameEntity.Player2GameEntry == GameEntry.Scissors)
                            return Result.Player2;
                        break;
                    case GameEntry.Scissors:
                        if (gameEntity.Player2GameEntry == GameEntry.Rock)
                            return Result.Player2;
                        else if (gameEntity.Player2GameEntry == GameEntry.Paper)
                            return Result.Player1;
                        break;
                }
            }

            return Result.Draw;
        }
        
    }
}
