﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPSGame
{
    public static class GameHelper
    {
        public enum GameEntry
        {
            Rock,
            Paper,
            Scissors
        }

        public enum Result
        {
            Draw,
            Player1,
            Player2
        }

        private static Random random = new Random();

        public static GameEntry GetRandomGameEntry()
        {
            var values = Enum.GetValues(typeof(GameEntry));
            return (GameEntry)values.GetValue(random.Next(values.Length));
        }

        public static string GetFriendlyString(this Result result)
        {
            string retVal = string.Empty;
            switch (result)
            {
                case Result.Draw:
                    retVal = "Draw";
                    break;
                case Result.Player1:
                    retVal = "Player 1 Wins!";
                    break;
                case Result.Player2:
                    retVal = "Player 2 Wins!";
                    break;
            }

            return retVal;
        }
    }
}
