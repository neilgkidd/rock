﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RPSGame
{
    public partial class GameUI : Form
    {
        private readonly IGameEntity gameEntity;
        private ProcessGame processGame;

        public GameUI()
        {
            InitializeComponent();
            setInitialDisplay();
            gameEntity = new GameEntity();
            processGame = new ProcessGame(gameEntity);
        }

        /// <summary>
        /// Clears the UI at start of the game
        /// </summary>
        private void setInitialDisplay()
        {
            pnlResult.Visible = false;
            pnlPlayer.Visible = false;
        }

        /// <summary>
        /// Updates the UI labels with the selected game entries
        /// </summary>
        private void updateLabels()
        {
            pnlResult.Visible = true;
            lblSelect.Text = gameEntity.Player1GameEntry.ToString();
            lblComputerSelected.Text = gameEntity.Player2GameEntry.ToString();
        }

        /// <summary>
        /// Run game logic
        /// </summary>
        private void playGame()
        {
            gameEntity.Player2GameEntry = GameHelper.GetRandomGameEntry();
            updateLabels();
            var winner = processGame.FindWinner();
            lblWinner.Text = winner.GetFriendlyString();
        }

        private void btnPlayer_Click(object sender, EventArgs e)
        {
            pnlPlayer.Visible = true;     
        }

        private void btnComputer_Click(object sender, EventArgs e)
        {
            pnlPlayer.Visible = false;
            
            gameEntity.Player1GameEntry = GameHelper.GetRandomGameEntry(); 
            playGame();
        }

        private void btnRock_Click(object sender, EventArgs e)
        {
            gameEntity.Player1GameEntry = GameHelper.GameEntry.Rock;
            playGame();
        }

        private void btnPaper_Click(object sender, EventArgs e)
        {
            gameEntity.Player1GameEntry = GameHelper.GameEntry.Paper;
            playGame();
        }

        private void btnScissors_Click(object sender, EventArgs e)
        {
            gameEntity.Player1GameEntry = GameHelper.GameEntry.Scissors;
            playGame();
        }       
    }
}
