﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RPSGame.GameHelper;

namespace RPSGame
{
    public class GameEntity : IGameEntity
    {
        public GameEntry Player1GameEntry { get; set; }

        public GameEntry Player2GameEntry { get; set; }

        public Result Winner { get; set; }
    }
}
