﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RPSGame.GameHelper;

namespace RPSGame
{
    public interface IGameEntity
    {
        GameEntry Player1GameEntry { get; set; }

        GameEntry Player2GameEntry { get; set; }

        Result Winner { get; set; }
    }
}
